"use strict";

const $ = require('jQuery');

// ELEMENTS
let resultWrapper = document.getElementById('results');
let $input = $('#search');
let $noResultsText = $('<h2>', {"class" : "text-center no-results", "text": "No results found. :("});

// SEARCH SETTINGS
let typingTimer;
let doneTypingInterval = 500;
let minLength = 3;
let text = $input.val();

// AJAX REQUEST
let method = 'POST';
let url = 'http://devbackend.awakenmycity.com/event/search';
let contentType = 'application/json';
let type = 'json';
let dictionary;

class EventListItem {
    constructor(data) {
        this.data = {};

        // collect data
        this.data.name = (data.name != null) ? data.name : '';
        this.data.author = (dictionary[data.author.user].full_name != null) ? dictionary[data.author.user].full_name : '';
        this.data.image = (data.url != null) ? data.url.s : '/assets/images/awaken-logo.png';


        // DOM element blocks
        // wrapper
        const wrapper = $( "<div>", {
            "class": "list-item"
        }),
            // thumbnail
            thumbnail = $("<figurecaption>", {
                "class" : "list-item__thumbnail",
                "style" : "background-image: url('" + this.data.image + "');"
            }),

            // content
            content = $("<article>", {
                "class" : "list-item__content",
            }),

                // title
                contentTitle = $('<h4>', {
                    "class" : "bold",
                    "text" : this.data.name
                }),

                // author
                contentAuthor = $('<p>', {
                    "text" : this.data.author
                });

        // build DOM element
        this.element =
            wrapper
            .append(thumbnail)
            .append(content
                .append(contentTitle)
                .append(contentAuthor));


        this.element[0].addEventListener('click', () => this.clickEvent(this.element));
    }

    clickEvent(el) {
        el.siblings( ".active" ).removeClass('active');

        if (el.hasClass('active')) {
            el.removeClass('active');
            window.alert(JSON.stringify(this.data));
        } else {
            el.addClass('active');
        }
    }
}

$input.on('keyup', function () {
    text = $input.val();
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

$input.on('keydown', function () {
    clearTimeout(typingTimer);
});

function doneTyping () {
    if ($input.val().length >= minLength) {
        loaderSet(true);
        resetResults();
        getData();
    } else {
        resetResults();
    }
}

function getData() {

    $.ajax({
        type: method,
        url: url,
        contentType: contentType,
        data: JSON.stringify({ "text": text }),
        dataType: type,
        cache: true,
        success: function(data){

            let totalResults = data.extras.total_results;
            dictionary = data.dictionary;

            if (totalResults != 0) {
                $noResultsText.remove();

                $.each(data.result, function(index, value) {

                    let eventListItem = new EventListItem(data.dictionary[value]);

                    renderBody(eventListItem.element);

                    if (index + 1 === totalResults) {
                        loaderSet(false);
                    }
                })
            } else {
                renderBody($noResultsText);
                loaderSet(false);
            }
        },
        error: function(reason){
            console.log("Failed: " + reason);
            loaderSet(false);
        }
    });
}


// add to wrapper
function renderBody(template) {
    template.appendTo(resultWrapper);
}

// clear DOM
function resetResults() {
    resultWrapper.innerHTML = '';
}

// loaderInit
function loaderSet(status) {
    if (status === true) {
        $('.loader').addClass('visible');
    }
    if (status === false) {
        $('.loader').removeClass('visible');
    }
}
