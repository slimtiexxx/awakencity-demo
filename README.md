# Awaken My City Demo

## Introduction

> This is a basic realtime JS searcher using jQuery, and ES6.

## View on the web
http://andrewsite.webuda.com/awaken-my-city/


## Installation

1. Clone the project `git clone https://gitlab.com/slimtiexxx/awakencity-demo.git`.
2. Go to project folder `cd awakencity-demo` and install dependencies via `npm install`.
3. Run `gulp watch` to run the project locally or `gulp build` to build the project.
